# Project Documentation Macros#

Free macros for project documentation in your Confluence Server wiki.

This repository contains the sources up to version 1.2.4 which support Confluence Server. Confluence Server is [available until February 15., 2024](https://www.atlassian.com/migration/assess/journey-to-cloud). 

Versions 1.3 supports Confluence Data Center on a new code base that is not contained in this repository.

## Overview

The following macros help to enhance your project documentation experience:

* [Definition List](https://www.smartics.eu/confluence/display/CONFMAC/Definition+List+Macro) - Renders term and definition information as a definition list. Currently Confluence does not easily support authors to write definition lists. But definition lists allow to render this form of information efficiently.
* [Section](https://www.smartics.eu/confluence/display/CONFMAC/Section+Macro) - Allows template authors to structure content without cluttering the document for readers.
* [Hide From Anonymous User](http://www.smartics.eu/confluence/display/CONFMAC/Hide+From+Anonymous+User+Macro) - Hides a text fragment from an anonymous user.
* [Hide From Viewer](https://www.smartics.eu/confluence/display/CONFMAC/Hide+From+Viewer+Macro) - Hides a text fragment from a user without edit rights. 

Our commercial [projectdoc Add-on](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence) contains versions of the [Definition List Macro](https://www.smartics.eu/confluence/display/PDAC1/Definition+List+Macro) and the [Section Macro](https://www.smartics.eu/confluence/display/PDAC1/Section+Macro) that provide additional features. These features use the infrastructure provided by projectdoc (e.g. [space properties](https://www.smartics.eu/confluence/display/PDAC1/Space+Properties) and [transclusion](https://www.smartics.eu/confluence/display/PDAC1/Transclusion+Macro)). For more information on projectdoc, please visit the [projectdoc website](https://www.smartics.eu/confluence/display/PDAC/)! 

## Resources

For more information please visit

* [Homepage](https://www.smartics.eu/confluence/display/CONFMAC) - For more information on this add-on and for giving feedback!
* [Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros) - To access the latest release to install on your Confluence server.
* [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) - For more information on projectdoc.
* [projectdoc Manual](https://www.smartics.eu/confluence/display/PDAC1/) - For more information on macros and blueprints provided by the commercial projectdoc Add-on.
* [JIRA](https://www.smartics.eu/jira/projects/CONFMAC) - For [Release Notes](https://www.smartics.eu/jira/projects/CONFMAC?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page) and activities.
