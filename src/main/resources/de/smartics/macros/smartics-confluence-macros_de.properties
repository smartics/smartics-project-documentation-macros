#
# Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# ### Macros ##################################################################

## Definition List Macro
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.label=Definition-List
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.desc=Stellt eine mehrspaltige HTML-Tabelle als HTML Definition-List dar.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.param.dlclass.label=CSS-Klasse f\u00fcr Definition-List
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.param.dlclass.desc=Wird an das 'dl' element als class-Attribute angef\u00fcgt.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.param.dtclass.label=CSS-Klasse f\u00fcr Definition-Term
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.param.dtclass.desc=Wird an das 'dt' element als class-Attribute angef\u00fcgt.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.param.ddclass.label=CSS-Klasse f\u00fcr Definition-Data
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-definition-list-macro.param.ddclass.desc=Wird an das 'dd' element als class-Attribute angef\u00fcgt.

projectdoc.macros.dl.error.cannotParseHtmlBody=Der K\u00f6rper des Macros \
  konnte nicht erfolgreich geparst werden. Es wird eine zweispaltige Tabelle \
  erwartet: Erst spalte enth\u00e4lt den Definition-Term, die zweite Spalte das \
  Definition-Data. Fehlermeldung: {0}
projectdoc.macros.dl.error.body.title=Keine Tabelle gefunden!
projectdoc.macros.dl.error.body.summary=Bitte f\u00fcgen Sie in den Makro-K\u00f6rper eine zweispaltige Tabelle ein.
projectdoc.macros.dl.error.body.contents=Die erste Spalte muss als Kopfzeilenzellen ausgezeichnet sein und die Definitionsnamen enthalten. Die zweite Datenspalte enth\u00e4lt die Definitionstexte.
projectdoc.macros.dl.error.body.ref=Unter <a href="https://www.smartics.eu/confluence/display/CONFMAC/Definition+List+Macro+in+Action">Definition List Macro in Action</a> finden Sie eine detaillierte Anleitung, inklusive eines Screencasts, in englischer Sprache.


## Hide From Anonymous User Macro
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide-from-anonymous-user-macro.label=Verstecke Inhalt vor anonymen Benutzern
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide-from-anonymous-user-macro.desc=Versteckt den Inhalt des Macros vor anonymen Benutzern
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide-from-anonymous-user-macro.param.replacement.label=Ersetzungstext
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide-from-anonymous-user-macro.param.replacement.desc=Text zur Darstellung f\u00fcr anonyome Benutzer.


## Hide Macro
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide.label=Verstecken-Makro
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide.desc=Versteckt ein Textfragment vor Lesern ohne Schreibrechte. \
  Dieses Verstecken ist lediglich eine Bequemlichkeit, jedoch kein sicherer Schutz von sensiblen Informationen. \
  Dieses Makro DARF NICHT verwendet werden, wenn die Textfragmente in Caches abgelegt werden. \
  Dies ist insbesondere in der Dokumenteigenschaftstabelle der Fall.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide.param.replacement.label=Ersetzungstext
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-hide.param.replacement.desc=Der Text der anstelle des versteckten Fragments angezeigt werden soll.


## Section Macro
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.label=Abschnitt
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.desc=Zeigt einen Abschnitt an, falls der Inhalt nicht leer ist.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.title.label=Titel
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.title.desc=Der Titel des Abschnitts.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.level.label=Ebene
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.level.desc=Die Ebene (startend mit 1) des Titels. Wird in eine HTML-Heading \u00fcbersetzt.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.intro-text.label=Einf\u00fchrender Text
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.intro-text.desc=Text der als Einf\u00fchrung angezeigt wird, wenn der Inhalt nicht leer ist.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.extro-text.label=Ausf\u00fchrender Text
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.extro-text.desc=Text der am Ende des Inhalts angezeigt wird, wenn der Inhalt nicht leer ist.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.hide.label=Verstecken
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.hide.desc=Erlaubt, den Abschnitt nicht anzuzeigen.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.ignore-template-buttons.label=Ignoriere Schaltfl\u00e4che f\u00fcr Vorlagen
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.param.ignore-template-buttons.desc=Wenn angew\u00e4hlt werden Schaltfl\u00e4chen zum Erzeugen von Dokumenten innerhalb des Abschnitts wie Leerzeichen behandelt.
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.body.label=Abschnittsinhalt
de.smartics.atlassian.confluence.smartics-atlassian-confluence-macros.projectdoc-section.body.desc=Der Inhalt des Abschnitts.
