/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.util;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import com.atlassian.confluence.content.render.xhtml.Namespace;
import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import com.atlassian.confluence.xml.XMLEntityResolver;

/**
 * Helper to do the actual parsing.
 */
public final class ConfluenceDocumentParserHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The factory to create XML parsers.
   */
  private static final SAXParserFactory XML_FACTORY =
      SAXParserFactory.newInstance();

  /**
   * The namespace prefix to construct an XML document from the passed in body.
   */
  public static final String DEFAULT_NAMESPACE_PREFIX = "xml";

  /**
   * The namespace map used for parsing.
   */
  private static final Map<String, String> NAMESPACE_MAP;

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  static {
    NAMESPACE_MAP = new ConcurrentHashMap<String, String>(
        XhtmlConstants.STORAGE_NAMESPACES.size());
    for (final Namespace namespace : XhtmlConstants.STORAGE_NAMESPACES) {
      NAMESPACE_MAP.put(namespace.getPrefix() != null ? namespace.getPrefix()
          : DEFAULT_NAMESPACE_PREFIX, namespace.getUri());
    }
  }

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  ConfluenceDocumentParserHelper() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static XPath createXPath(final Document macroBodyDoc,
      final String xPath) {
    final XPath xpath = macroBodyDoc.createXPath(xPath);
    xpath.setNamespaceURIs(NAMESPACE_MAP);
    return xpath;
  }

  public static Document createXmlDocument(
      final String xhtmlFragmentFromConfluencePage)
          throws DocumentException, UnsupportedEncodingException,
          ParserConfigurationException, SAXException {
    final String xmlDocumentString =
        createXmlDocumentString(xhtmlFragmentFromConfluencePage);
    final ByteArrayInputStream input =
        new ByteArrayInputStream(xmlDocumentString.getBytes("UTF-8"));
    try {
      final XMLEntityResolver resolver = new XhtmlTransitionalResolver();
      final SAXParser saxParser = XML_FACTORY.newSAXParser();
      final SAXReader saxReader =
          new SAXReader(saxParser.getXMLReader(), false);
      saxReader.setFeature(
          "http://apache.org/xml/features/validation/unparsed-entity-checking",
          false);
      saxReader.setFeature(
          "http://apache.org/xml/features/nonvalidating/load-external-dtd",
          true);
      saxReader.setEntityResolver(resolver);
      final Document document = saxReader.read(input);

      return document;
    } finally {
      IOUtils.closeQuietly(input);
    }
  }

  private static String createXmlDocumentString(
      final String xmlFragmentString) {
    final StringBuilder buffer =
        new StringBuilder(xmlFragmentString.length() + 2048);
    buffer.append(
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\""
            + " \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<xhtml");

    for (final Namespace namespace : XhtmlConstants.STORAGE_NAMESPACES) {
      buffer.append(" xmlns");
      if (!namespace.isDefaultNamespace()) {
        buffer.append(":").append(namespace.getPrefix());
      }
      buffer.append("=\"").append(namespace.getUri()).append("\"");
    }

    final String fixedXmlFragmentString = fixXhtmlFragment(xmlFragmentString);

    buffer.append(">").append(fixedXmlFragmentString).append("</xhtml>");
    return buffer.toString();
  }

  /**
   * WORKAROUND: Attributes are not properly separated by spaces.
   *
   * @param xhtmlFragment the fragment to fix.
   * @return the fixed fragment.
   */
  public static final String fixXhtmlFragment(final String xhtmlFragment) {
    final String fixedXhtmlFragment =
        xhtmlFragment.replace("'data-", "' data-");
    return fixedXhtmlFragment;
  }

  // --- object basics --------------------------------------------------------

}
