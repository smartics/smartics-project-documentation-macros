/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.util;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

/**
 * Base implementation to unify the different macro APIs.
 */
public abstract class UnifiedApiMacro extends BaseMacro implements Macro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  protected UnifiedApiMacro() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public boolean isInline() {
    return getOutputType() == OutputType.INLINE;
  }

  @Override
  public boolean hasBody() {
    return getBodyType() != Macro.BodyType.NONE;
  }

  // --- business -------------------------------------------------------------

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public String execute(final Map parameters, final String body,
      final RenderContext renderContext) throws MacroException {
    try {
      return execute(parameters, body,
          new DefaultConversionContext(renderContext));
    } catch (final MacroExecutionException e) {
      throw new MacroException(e);
    }
  }

  protected static String getParameter(final Map<String, String> parameters,
      final String name, final String defaultValue) {
    final String value = parameters.get(name);
    if (value == null) {
      return defaultValue;
    } else {
      return value;
    }
  }

  protected static boolean getParameterAsBoolean(
      final Map<String, String> parameters, final String paramName,
      final String defaultValue) {
    final String valueString =
        getParameter(parameters, paramName, defaultValue);
    final boolean value = Boolean.parseBoolean(valueString);
    return value;
  }

  protected static int getParameterAsInt(final Map<String, String> parameters,
      final String paramName, final String defaultValue) {
    final String valueString =
        getParameter(parameters, paramName, defaultValue);
    final int value = Integer.parseInt(valueString);
    return value;
  }

  /**
   * Returns the authenticated user from the thread-local context.
   *
   * @return the authenticated user from the thread-local context.
   */
  protected static ConfluenceUser getAuthenticatedUser() {
    return AuthenticatedUserThreadLocal.get();
  }

  // --- object basics --------------------------------------------------------

}
