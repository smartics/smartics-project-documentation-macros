/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.util;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * Normalizes body content to check for its emptiness
 */
public class EmptyPageBodyHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG =
      LoggerFactory.getLogger(EmptyPageBodyHelper.class);

  /**
   * Matches template buttons.
   */
  private static final Pattern TEMPLATE_MACRO_PATTERN = Pattern.compile(
      "(<a[^>]+create-from-template-button[^>]+>)([\\s\\S]*?)(</a>)",
      Pattern.CASE_INSENSITIVE);

  private static final String XPATH_CONFLUENCE_CHECKBOXES =
      "//" + ConfluenceDocumentParserHelper.DEFAULT_NAMESPACE_PREFIX
          + ":ul[@class='inline-task-list']/"
          + ConfluenceDocumentParserHelper.DEFAULT_NAMESPACE_PREFIX + ":li";

  // --- members --------------------------------------------------------------

  private final boolean ignoreTemplateButtons;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public EmptyPageBodyHelper() {
    this(false);
  }

  /**
   * Default constructor.
   */
  public EmptyPageBodyHelper(final boolean ignoreTemplateButtons) {
    this.ignoreTemplateButtons = ignoreTemplateButtons;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Checks if the body is empty. It is empty if it is a blank string, is a
   * non-breaking space or if it contains an empty paragraph element.
   *
   * @param body the body to check.
   * @return <code>true</code> if it is empty, <code>false</code> if it is not.
   */
  public boolean isEmpty(final String body) {
    String normBody = body;

    boolean isEmpty = simpleIsEmptyChecks(normBody);
    if (isEmpty) {
      return true;
    }

    if (ignoreTemplateButtons) {
      normBody = removeTemplateButtons(normBody);
      isEmpty = simpleIsEmptyChecks(normBody);
      if (isEmpty) {
        return true;
      }
    }

    return isEmptyXml(normBody);
  }

  private boolean isEmptyXml(final String body) {
    try {
      final Document document = parseXml(body);
      final boolean hasText = hasText(document);
      if (hasText) {
        return false;
      }

      final boolean hasNonEmptyElements = hasNonEmptyElements(document);
      return !hasNonEmptyElements;
    } catch (final Exception e) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Cannot parse body content: " + body, e);
      }
      return false;
    }
  }

  private static boolean simpleIsEmptyChecks(final String body) {
    boolean isEmpty = StringUtils.isBlank(body);
    if (isEmpty) {
      return true;
    }

    isEmpty = isNonBreakingSpace(body);
    if (isEmpty) {
      return true;
    }

    return false;
  }

  private static boolean isNonBreakingSpace(final String input) {
    if (input.length() == 1) {
      final char character = input.charAt(0);
      return Character.isWhitespace(character) || (character) == 160;
    }

    return false;
  }

  private static String removeTemplateButtons(final String body) {
    final Matcher matcher = TEMPLATE_MACRO_PATTERN.matcher(body);
    final String cleaned = matcher.replaceAll("");
    return cleaned;
  }

  public final Document parseXml(final String body)
      throws UnsupportedEncodingException, DocumentException,
      ParserConfigurationException, SAXException {
    final Document document =
        ConfluenceDocumentParserHelper.createXmlDocument(body);
    return document;
  }

  private boolean hasNonEmptyElements(final Document document) {
    final XPath xpath = ConfluenceDocumentParserHelper.createXPath(document,
        XPATH_CONFLUENCE_CHECKBOXES);
    final Element rootElement = (Element) xpath.selectSingleNode(document);
    return rootElement != null;
  }

  public static boolean hasText(final Document document) {
    final Element root = document.getRootElement();
    return hasText(root);
  }

  public static boolean hasText(final Element root) {
    final String text = root.getText();
    if (!simpleIsEmptyChecks(text)) {
      return true;
    }

    for (final Object node : root.elements()) {
      if (node instanceof Element) {
        final boolean hasText = hasText((Element) node);
        if (hasText) {
          return true;
        }
      }
    }

    return false;
  }

  // --- object basics --------------------------------------------------------

}
