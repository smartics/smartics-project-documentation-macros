/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.IOUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.atlassian.confluence.xml.XhtmlEntityResolver;
import com.atlassian.core.util.ClassLoaderUtils;

/**
 * Provides DTDs we use for parsing XHTML. Adds
 * <code>-//W3C//DTD XHTML 1.0 Transitional//EN</code> to be locally available.
 * <p>
 * As an extension to {@link XhtmlEntityResolver}, this implementation reuses
 * code from this class.
 * </p>
 */
public class XhtmlTransitionalResolver extends XhtmlEntityResolver {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Maps public entity ids to the resource that defines it.
   */
  private final Map<String, String> publicIdToEntityMap =
      new HashMap<String, String>(1);

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public XhtmlTransitionalResolver() {
    try {
      publicIdToEntityMap.put("-//W3C//DTD XHTML 1.0 Transitional//EN",
          readResource("xhtml1-transitional.dtd"));
    } catch (final IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Object resolveEntity(final String publicID, final String systemID,
      final String baseURI, final String namespace) throws XMLStreamException {
    final String resource = publicIdToEntityMap.get(publicID);

    if (resource == null) {
      return super.resolveEntity(publicID, systemID, baseURI, namespace);
    }

    return new ByteArrayInputStream(resource.getBytes());
  }

  @Override
  public InputSource resolveEntity(final String publicId, final String systemId)
      throws SAXException, IOException {
    final String resource = publicIdToEntityMap.get(publicId);

    if (resource == null) {
      return super.resolveEntity(publicId, systemId);
    }

    return new InputSource(new StringReader(resource));
  }

  private String readResource(final String name) throws IOException {
    final String resourceName = "de/smartics/macros/xhtml/" + name;
    final InputStream input = ClassLoaderUtils.getResourceAsStream(resourceName,
        XhtmlTransitionalResolver.class);
    if (input == null) {
      throw new IOException("The resource " + resourceName
          + " has not been found on the classpath.");
    }

    try {
      final String resource = IOUtils.toString(input, "UTF-8");
      return resource;
    } finally {
      input.close();
    }
  }

  // --- object basics --------------------------------------------------------

}
