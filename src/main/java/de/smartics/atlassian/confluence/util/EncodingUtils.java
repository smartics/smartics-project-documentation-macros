/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.util;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ElementIdCreator;
import com.atlassian.confluence.content.render.xhtml.HtmlElementIdCreator;
import com.atlassian.confluence.renderer.PageContext;

/**
 * Helper to encode values. We should really use
 * <a href="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project">OWASP
 * Java Encoder Project</a>, but would have to add the lib to the dependencies.
 * <p>
 * The methods herein only check the most obvious, but using them will help to
 * migrate to the OWASP library.
 * </p>
 */
public final class EncodingUtils {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The key to access the HTML element ID creator in the conversion context.
   */
  public static final String HTML_ELEMENT_ID_CREATOR =
      "de.smartics.projectdoc.htmlElementIdCreator";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Utils pattern.
   */
  private EncodingUtils() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static String normalizeHtmlElementId(final ConversionContext context,
      final String basis) {
    final ElementIdCreator creator = fetchOrCreateCreator(context);
    final String html5Id = creator.generateId(basis);
    return html5Id;
  }

  public static String normalizeHtmlElementId(final String uniqueId) {
    final String html5Id = HtmlElementIdCreator.convertToIdHtml5(uniqueId);
    return html5Id;
  }

  private static ElementIdCreator fetchOrCreateCreator(
      final ConversionContext conversionContext) {
    // We copy the element id creator instance to the conversion context to
    // replace it if required. In some situations we want to render a page with
    // its own context, in others we want to inject the element id creator
    // instance from an already existing context.
    ElementIdCreator creator = (ElementIdCreator) conversionContext
        .getProperty(HTML_ELEMENT_ID_CREATOR);
    if (creator == null) {
      final PageContext pageContext = conversionContext.getPageContext();
      if (pageContext != null) {
        creator = pageContext.getElementIdCreator();
      }

      if (creator == null) {
        creator = new HtmlElementIdCreator();
      }

      conversionContext.setProperty(HTML_ELEMENT_ID_CREATOR, creator);
    }
    return creator;
  }

  public static String encodeHtmlAttributeValue(final String value) {
    final String encoded =
        StringUtils.replaceEach(value, new String[] {"&", "<", "\"", "'"},
            new String[] {"&amp;", "&lt;", "&#34;", "&#39;"});
    return encoded;
  }

  // --- object basics --------------------------------------------------------

}
