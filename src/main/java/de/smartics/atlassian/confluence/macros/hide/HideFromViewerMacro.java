/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.macros.hide;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.user.User;

import de.smartics.atlassian.confluence.util.UnifiedApiMacro;

/**
 * Allows to not render information, if the viewer not also has editing rights
 * on the page.
 * <p>
 * Please note that the macro MUST NOT BE USED in places that are prerendered
 * and cached. This is especially the case with projectdoc document properties.
 * </p>
 */
public class HideFromViewerMacro extends UnifiedApiMacro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name of the parameter that contains the text to use as a replacement
   * for the hidden text.
   */
  protected static final String PARAM_REPLACEMENT = "replacement";

  // --- members --------------------------------------------------------------

  private final PermissionManager permissionManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public HideFromViewerMacro(final PermissionManager permissionManager) {
    this.permissionManager = permissionManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public BodyType getBodyType() {
    return BodyType.PLAIN_TEXT;
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.INLINE;
  }

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext context) throws MacroExecutionException {
    final ContentEntityObject entity = context.getEntity();
    final User user = getAuthenticatedUser();
    final boolean edit = user != null
        && permissionManager.hasPermission(user, Permission.EDIT, entity);
    if (!edit) {
      final String text = getParameter(parameters, PARAM_REPLACEMENT, "******");
      return text;
    }

    return body;
  }

  // --- object basics --------------------------------------------------------

}
