/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.macros.dl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Helper to create a definition list.
 */
final class DefinitionListBuilder {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The buffer to create the HTML definition list.
   */
  private final StringBuilder buffer;

  /**
   * The CSS classes to add to the HTML definition list element.
   */
  private final String dlclass;

  /**
   * The CSS classes to add to the HTML definition term elements.
   */
  private final String dtclass;

  /**
   * The CSS classes to add to the HTML definition data elements.
   */
  private final String ddclass;

  /**
   * The counter for the number of terms that have been added.
   */
  private int termCounter;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private DefinitionListBuilder(final Builder builder) {
    this.buffer = builder.buffer;
    this.dlclass = builder.dlclass;
    this.dtclass = builder.dtclass;
    this.ddclass = builder.ddclass;
    init();
  }

  // ****************************** Inner Classes *****************************

  /**
   * The builder of definition list builders.
   */
  static final class Builder {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    /**
     * The buffer to create the HTML definition list.
     */
    private StringBuilder buffer;

    /**
     * The CSS classes to add to the HTML definition list element.
     */
    private String dlclass;

    /**
     * The CSS classes to add to the HTML definition term elements.
     */
    private String dtclass;

    /**
     * The CSS classes to add to the HTML definition data elements.
     */
    private String ddclass;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    Builder withBuffer(final StringBuilder buffer) {
      this.buffer = buffer;
      return this;
    }

    Builder withDlclass(final String dlclass) {
      this.dlclass = dlclass;
      return this;
    }

    Builder withDtclass(final String dtclass) {
      this.dtclass = dtclass;
      return this;
    }

    Builder withDdclass(final String ddclass) {
      this.ddclass = ddclass;
      return this;
    }

    // --- business -----------------------------------------------------------

    DefinitionListBuilder build() {
      if (buffer == null) {
        buffer = new StringBuilder(2048);
      }
      if (StringUtils.isEmpty(dlclass)) {
        dlclass = null;
      }
      if (StringUtils.isEmpty(dtclass)) {
        dtclass = null;
      }
      if (StringUtils.isEmpty(ddclass)) {
        ddclass = null;
      }

      return new DefinitionListBuilder(this);
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  private void init() {
    buffer.append("<dl");
    appendCssClass(dlclass);
    buffer.append(">\n");
  }

  private void close() {
    buffer.append("</dl>");
  }

  // --- get&set --------------------------------------------------------------

  boolean hasTerms() {
    return termCounter > 0;
  }

  // --- business -------------------------------------------------------------

  void addDefinition(final String term, final String data) {
    addDefinition(term);
    addDefinitionData(data);
  }

  void addDefinition(final String term) {
    termCounter++;
    buffer.append("  <dt");
    appendCssClass(dtclass);
    buffer.append(">").append(term).append("</dt>");
  }

  void addDefinitions(final String term, final List<String> datas) {
    addDefinition(term);
    for (final String data : datas) {
      addDefinitionData(data);
    }
    buffer.append('\n');
  }

  private void addDefinitionData(final String data) {
    buffer.append("<dd");
    appendCssClass(ddclass);
    buffer.append('>').append(data).append("</dd>");
  }

  private void appendCssClass(final String cssClass) {
    if (cssClass != null) {
      buffer.append(" class=\"").append(cssClass).append('"');
    }
  }

  String createContent() {
    close();

    return buffer.toString();
  }

  // --- object basics --------------------------------------------------------

}
