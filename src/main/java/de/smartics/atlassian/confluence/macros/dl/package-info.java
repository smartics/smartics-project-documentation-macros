/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Provides the definition list macro that renders a multi column table as an
 * HTML definition list.
 * <p>
 * The first column is expected to be the definition term, rendered as a
 * <tt>th</tt> element. Any column that follows is expected to be a <tt>td</tt>
 * element that will be rendered as a definition data element.
 * </p>
 */
package de.smartics.atlassian.confluence.macros.dl;
