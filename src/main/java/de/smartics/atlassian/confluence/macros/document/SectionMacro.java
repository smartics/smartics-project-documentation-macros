/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.macros.document;

import static de.smartics.atlassian.confluence.util.EncodingUtils.encodeHtmlAttributeValue;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.RenderMode;

import de.smartics.atlassian.confluence.util.EmptyPageBodyHelper;
import de.smartics.atlassian.confluence.util.EncodingUtils;
import de.smartics.atlassian.confluence.util.UnifiedApiMacro;

/**
 * A macro that hides any contents, if the body is empty.
 *
 * @since 1.1
 */
public class SectionMacro extends UnifiedApiMacro implements Macro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name to the parameter that contains the title to be shown above the
   * content.
   */
  private static final String PARAM_TITLE = "title";

  /**
   * The name to the parameter that contains indicates the level of the section
   * header (starting with 1).
   */
  private static final String PARAM_LEVEL = "level";

  /**
   * The name to the parameter that contains the introduction text to be
   * rendered in a paragraph after the headline.
   * <p>
   * The text is only rendered, if the body is empty.
   * </p>
   */
  private static final String PARAM_INTRO = "intro-text";

  /**
   * The name to the parameter that contains a text to be rendered in a
   * paragraph before the end of the section.
   * <p>
   * The text is only rendered, if the body is empty.
   * </p>
   */
  private static final String PARAM_EXTRO = "extro-text";

  /**
   * The name to the parameter that controls if the content should be hidden.
   */
  private static final String PARAM_HIDE = "hide";

  /**
   * The name to the parameter that controls whether to ignore template button
   * macros.
   */
  private static final String PARAM_IGNORE_TEMPLATE_BUTTONS =
      "ignore-template-buttons";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public SectionMacro() {}

  // ****************************** Inner Classes *****************************

  /**
   * The parsed parameters.
   */
  private final class ContentRenderContext {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final String body;

    private String pageTitle;

    private int idCounter;

    private final ConversionContext context;

    private String htmlId;

    private final Map<String, String> parameters;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    protected ContentRenderContext(final Map<String, String> parameters,
        final String body, final ConversionContext context) {
      this.parameters = parameters;
      this.body = body;
      this.context = context;
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    protected String getHtmlId() {
      if (htmlId == null) {
        final String title = getParameter(parameters, PARAM_TITLE, null);
        final String sectionId = title;
        final String uniqueSectionIdForTransclusion =
            pageTitle + '-' + sectionId;
        htmlId = EncodingUtils.normalizeHtmlElementId(context,
            uniqueSectionIdForTransclusion);
      }

      return htmlId;
    }

    // --- business -----------------------------------------------------------

    protected StringBuilder render(final StringBuilder buffer) {
      final boolean hide =
          getParameterAsBoolean(parameters, PARAM_HIDE, "false");
      if (hide) {
        return buffer;
      }

      final String anchor = getHtmlId();
      if (StringUtils.isNotBlank(anchor)) {
        buffer.append("<span class=\"confluence-anchor-link\" id=\"")
            .append(encodeHtmlAttributeValue(anchor)).append("\">")
            .append("</span>");
      }
      renderBody(buffer, anchor);

      return buffer;
    }

    protected StringBuilder renderBody(final StringBuilder buffer,
        final String htmlAnchor) {
      final String title = getParameter(parameters, PARAM_TITLE, null);
      if (title == null) {
        buffer.append(body);
        return buffer;
      }
      final int level = getParameterAsInt(parameters, PARAM_LEVEL, "1");
      final String normalizedHtmlAnchor = encodeHtmlAttributeValue(
          htmlAnchor + (idCounter > 1 ? "." + (idCounter - 1) : ""));
      renderSectionSuroundingDivStart(buffer, level, normalizedHtmlAnchor);
      renderSpanForCompatabilityToConfluenceCSS(buffer);
      renderHeading(buffer, title, level, normalizedHtmlAnchor);
      renderContent(buffer);
      renderSectionSuroundingDivEnd(buffer);

      return buffer;
    }

    private void renderContent(final StringBuilder buffer) {
      renderText(parameters, buffer, PARAM_INTRO);
      buffer.append(body);
      renderText(parameters, buffer, PARAM_EXTRO);
    }

    private void renderHeading(final StringBuilder buffer, final String title,
        final int level, final String normalizedHtmlId) {
      buffer.append("<h").append(level).append(" id=\"")
          .append(normalizedHtmlId).append("\" ");
      buffer.append('>').append(title).append("</h").append(level)
          .append(">\n");
    }

    private void renderSpanForCompatabilityToConfluenceCSS(
        final StringBuilder buffer) {
      buffer.append("<span></span>");
    }

    private void renderSectionSuroundingDivEnd(final StringBuilder buffer) {
      buffer.append("</div>");
    }

    private void renderSectionSuroundingDivStart(final StringBuilder buffer,
        final int level, final String normalizedHtmlIdPart) {
      buffer.append("<div class=\"").append("heading" + level).append("\" ")
          .append("id=\"").append("div-" + normalizedHtmlIdPart).append("\">");
    }

    private void renderText(final Map<String, String> parameters,
        final StringBuilder buffer, final String key) {
      final String text = getParameter(parameters, key, null);
      if (StringUtils.isNotBlank(text)) {
        buffer.append("<p>").append(text).append("</p>\n");
      }
    }

    private boolean isHide() {
      final boolean hide =
          getParameterAsBoolean(parameters, PARAM_HIDE, "false");
      return hide;
    }

    // --- object basics ------------------------------------------------------

  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public BodyType getBodyType() {
    return BodyType.RICH_TEXT;
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.BLOCK;
  }

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.ALL;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext context) throws MacroExecutionException {
    final ContentRenderContext rendererContext =
        createParameters(parameters, body, context);

    rendererContext.pageTitle = context.getPageContext().getPageTitle();
    // final String spaceKey = context.getPageContext().getSpaceKey();

    if (!rendererContext.isHide()) {
      final StringBuilder buffer = new StringBuilder(8192);
      rendererContext.render(buffer);
      final String renderedBody = buffer.toString();

      final boolean ignoreTemplateButtons = getParameterAsBoolean(parameters,
          PARAM_IGNORE_TEMPLATE_BUTTONS, "false");
      final boolean isEmpty =
          new EmptyPageBodyHelper(ignoreTemplateButtons).isEmpty(body);
      if (!isEmpty) {
        return renderedBody;
      }
    }

    return StringUtils.EMPTY;
  }

  private ContentRenderContext createParameters(
      final Map<String, String> parameters, final String body,
      final ConversionContext context) {
    return new ContentRenderContext(parameters, body, context);
  }

  // --- object basics --------------------------------------------------------

}
