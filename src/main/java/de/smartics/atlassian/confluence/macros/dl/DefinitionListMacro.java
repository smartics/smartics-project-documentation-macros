/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.macros.dl;

import de.smartics.atlassian.confluence.util.UnifiedApiMacro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Locale;
import java.util.Map;

/**
 * Renders a multi column table as a definition list.
 * <p>
 * The first column is expected to be the definition term, rendered as a
 * <tt>th</tt> element. Any column that follows is expected to be a <tt>td</tt>
 * element that will be rendered as a definition data element.
 * </p>
 *
 * @since 1.0
 */
public class DefinitionListMacro extends UnifiedApiMacro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The helper to access the locale.
   */
  private final LocaleManager localeManager;

  /**
   * The helper to access localized information.
   */
  private final I18NBeanFactory i18nBeanFactory;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DefinitionListMacro(final LocaleManager localeManager,
      final I18NBeanFactory i18nBeanFactory) {
    this.localeManager = localeManager;
    this.i18nBeanFactory = i18nBeanFactory;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public BodyType getBodyType() {
    return BodyType.RICH_TEXT;
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.BLOCK;
  }

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.ALL;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext context) throws MacroExecutionException {
    final DefinitionListBuilder builder =
        createDefinitionListBuilder(parameters);
    final TableParser parser = new TableParser(builder);
    try {
      parser.parse(body);

      if (!builder.hasTerms()) {
        return renderHelpAndErrorPage();
      }

      final String renderedBody = builder.createContent();
      return renderedBody;
    } catch (final Exception e) {
      final Locale locale = localeManager.getSiteDefaultLocale();
      final I18NBean i18n = i18nBeanFactory.getI18NBean(locale);
      final String message = e.getMessage();
      final String errorMessage =
          RenderUtils
              .blockError(
                  i18n.getText("projectdoc.macros.dl.error.cannotParseHtmlBody",
                      new String[] {StringEscapeUtils.escapeHtml4(message)}),
                  "");
      return errorMessage;
    }
  }

  private String renderHelpAndErrorPage() {
    final Locale locale = localeManager.getSiteDefaultLocale();
    final I18NBean i18n = i18nBeanFactory.getI18NBean(locale);

    final StringBuilder buffer = new StringBuilder(1024);

    final String title = i18n.getText("projectdoc.macros.dl.error.body.title");
    final String summary =
        i18n.getText("projectdoc.macros.dl.error.body.summary");
    final String contents =
        i18n.getText("projectdoc.macros.dl.error.body.contents");
    final String ref = i18n.getText("projectdoc.macros.dl.error.body.ref");
    buffer.append(
        "<div class=\"aui-message error projectdoc-box shadowed information-macro\" data-projectdoc-box-type=\"error\">\n");
    buffer.append("  <div class=\"box-icon box-icon-error\"></div>\n");
    buffer.append("  <div class=\"title\">").append(title).append("</div>\n");
    buffer.append("  <div class=\"message-content\"><p><strong><i>")
        .append(summary)
        .append("</i></strong></p>\n")
        .append("<p>")
        .append(contents)
        .append("</p>")
        .append("<p>")
        .append(ref)
        .append("</p>\n");
    buffer.append("  </div>\n</div>\n");

    final String error = buffer.toString();
    return error;
  }

  private DefinitionListBuilder createDefinitionListBuilder(
      final Map<String, String> parameters) {
    final DefinitionListBuilder.Builder builderBuilder =
        new DefinitionListBuilder.Builder();
    builderBuilder
        .withDlclass(getParameter(parameters, "dlclass", "simpleindent"));
    builderBuilder.withDtclass(getParameter(parameters, "dtclass", null));
    builderBuilder.withDdclass(getParameter(parameters, "ddclass", null));
    final DefinitionListBuilder builder = builderBuilder.build();
    return builder;
  }

  // --- object basics --------------------------------------------------------

}
