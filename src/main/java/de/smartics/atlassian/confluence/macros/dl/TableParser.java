/*
 * Copyright 2014-2023 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.atlassian.confluence.macros.dl;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper to do the actual HTML table parsing.
 */
final class TableParser {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final DefinitionListBuilder builder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  TableParser(final DefinitionListBuilder builder) {
    this.builder = builder;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void parse(final String pageBody) throws IOException {
    final Document document = Jsoup.parse(pageBody);
    configureHtmlParsing(document);

    final Elements elements = document.select("table > tbody");
    final Element rootElement = elements.first();

    if (rootElement == null) {
      return;
    }

    final List<Element> rowElements = rootElement.getElementsByTag("tr");
    if (rowElements != null) {
      loadDocumentPropertiesFromTableRows(rowElements);
    }
  }

  private void loadDocumentPropertiesFromTableRows(
      final List<Element> rowElements) throws IOException {
    for (final Element rowElement : rowElements) {
      final List<Element> ths = rowElement.getElementsByTag("th");
      if (ths.size() != 1) {
        continue;
      }
      final Element definitionTermElement = ths.get(0);
      final String definitionTerm = getInnerHtml(definitionTermElement);

      final List<Element> tds = rowElement.getElementsByTag("td");
      if (tds.isEmpty()) {
        builder.addDefinition(definitionTerm);
      }

      if (tds.size() == 1) {
        final Element definitionDataElement = tds.get(0);
        final String definitionData = getInnerHtml(definitionDataElement);
        if (StringUtils.isNotBlank(definitionData)) {
          builder.addDefinition(definitionTerm, definitionData);
        } else {
          builder.addDefinition(definitionTerm);
        }
      } else {
        final List<String> definitionDatas = new ArrayList<String>(tds.size());
        for (final Element tdElement : tds) {
          final String definitionData = getInnerHtml(tdElement);
          if (!isBlankValue(definitionData)) {
            definitionDatas.add(definitionData);
          }
        }

        if (!definitionDatas.isEmpty()) {
          builder.addDefinitions(definitionTerm, definitionDatas);
        } else {
          builder.addDefinition(definitionTerm);
        }
      }
    }
  }

  private static void configureHtmlParsing(final Document document)
      throws NullPointerException {
    document.outputSettings(document.outputSettings()
        .prettyPrint(false));
  }

  private static final boolean isBlankValue(final String value) {
    return StringUtils.isBlank(value)
        || (value.length() == 1 && 160 == value.charAt(0))
        || "&nbsp;".equals(value) || containsOnlyWhitespaceTags(value);
  }

  private static boolean containsOnlyWhitespaceTags(String value) {
    final String testValue = value.replaceAll("<br>", "")
        .replaceAll("</br>", "")
        .replaceAll("<br/>", "")
        .replaceAll(
            "<br xmlns=\"http://www.w3.org/1999/xhtml\" clear=\"none\"/>", "")
        .replaceAll(
            "<br xmlns=\"http://www.w3.org/1999/xhtml\" clear=\"none\">", "");
    return StringUtils.isBlank(testValue);
  }

  private String getInnerHtml(final Element element) throws IOException {
    if (element == null) {
      return StringUtils.EMPTY;
    }
    return element.html();
  }

  // --- object basics --------------------------------------------------------

}
